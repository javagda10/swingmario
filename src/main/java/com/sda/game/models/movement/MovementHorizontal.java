package com.sda.game.models.movement;

public enum MovementHorizontal {
    NONE, LEFT, RIGHT;
}

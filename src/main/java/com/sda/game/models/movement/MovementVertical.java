package com.sda.game.models.movement;

public enum MovementVertical {
    NONE, UP, DOWN;
}

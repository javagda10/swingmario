package com.sda.game.models;

import com.sda.game.events.Dispatcher;
import com.sda.game.interfaces.IRewardListener;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Score extends AbstractPaintable implements IRewardListener {
    private int lives;
    private int coinsCount;

    private BufferedImage heart;

    public Score() {
        super(0, 0);
        this.lives = 3;
        this.coinsCount = 0;

        try {
            heart = ImageIO.read(new File("images/Pixel_heart_icon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Dispatcher.instance.registerObject(this);
    }

    @Override
    public void paint(Graphics2D pióro, int offset) {
        pióro.setColor(Color.black);
        pióro.setFont(new Font("Arial", Font.BOLD, 18));
        pióro.drawString("Coins: " + coinsCount, 0, 50);

        for (int i = 0; i < lives; i++) {
            pióro.drawImage(heart, 0 + i * 30, 0, null);
        }
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public void reward(AbstractPaintable paintable) {
        if (paintable instanceof Coin) {
            coinsCount++;
        }
        if (coinsCount >= 100) {
            coinsCount = 0;
            lives++;
        }
    }
}

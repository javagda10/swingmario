package com.sda.game.views;

import com.sda.game.events.*;
import com.sda.game.interfaces.IFinishGateReachedListener;
import com.sda.game.map.MapReader;
import com.sda.game.models.AbstractPaintable;
import com.sda.game.models.GameHero;
import com.sda.game.models.Gate;
import com.sda.game.models.Score;
import com.sda.game.models.movement.MovementHorizontal;
import com.sda.game.models.movement.MovementVertical;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.io.IOException;

public class MainPanel extends JPanel implements IFinishGateReachedListener {
    private Dimension wymiar;
    private long lastMove;
    private static final double PIXELS_PER_SECOND = 200;
    private int drawOffset = 0;
    private int level = 1;

    private GameHero hero = new GameHero(400, 300);
    private Score score = new Score();
    private MovementVertical vertical;
    private MovementHorizontal horizontal;

    private List<AbstractPaintable> przeszkody;
    private List<AbstractPaintable> nagrody;

    private Gate finishGate;

    public MainPanel(Dimension wymiar) {
        super();

        setSize(wymiar);
        setPreferredSize(wymiar);
        setMinimumSize(wymiar);
        setMaximumSize(wymiar);

        this.wymiar = wymiar;

        loadLevel();

        lastMove = System.currentTimeMillis();
        Dispatcher.instance.registerObject(this);
    }

    private void loadLevel() {
        MapReader reader = new MapReader("mapy/level" + level + ".map", wymiar.height);
        try {
            reader.loadMap();
            przeszkody = reader.getElementyPrzeszkodyMapy();
            nagrody = reader.getElementyNagrodyMapy();
            finishGate = reader.getGate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D pióro = (Graphics2D) g;
        pióro.setColor(Color.CYAN);
        pióro.fillRect(0, 0, wymiar.width, wymiar.height);

        finishGate.paint(pióro, drawOffset);

        hero.paint(pióro, drawOffset);

        for (AbstractPaintable paintable : przeszkody) {
            paintable.paint(pióro, drawOffset);
        }

        for (AbstractPaintable paintable : nagrody) {
            paintable.paint(pióro, drawOffset);
        }

        score.paint(pióro, drawOffset);
    }

    /**
     * Metoda poruszania postaciami na scenie
     */
    public void moveObjectsOnScene() {
        long difference = System.currentTimeMillis() - lastMove;
        lastMove = System.currentTimeMillis();

        double moveStep = (PIXELS_PER_SECOND * difference) / 1000.0;

        boolean maCosPodSoba = false;

        for (AbstractPaintable paintable : przeszkody) {
            if (hero.checkCollision(paintable)) {
                Dispatcher.instance.dispatch(new VerticalCollisionEvent(paintable));
            }
            if (!hero.canMoveDown(paintable)) {
                maCosPodSoba = true;
            }
        }
        if (!maCosPodSoba) {
            hero.fall();
        }

        hero.moveVertical(moveStep);

        List<AbstractPaintable> kopia = new ArrayList<>(nagrody);

        for (AbstractPaintable paintable : kopia) {
            if (hero.checkCollision(paintable)) {
                Dispatcher.instance.dispatch(new RewardCollisionEvent(paintable));
                nagrody.remove(paintable);
            }
        }


        boolean canMoveHorizontal = true;
        if (horizontal == MovementHorizontal.LEFT) {
            for (AbstractPaintable przeszkoda : przeszkody) {
                if (!hero.canMoveLeft(przeszkoda)) {
                    canMoveHorizontal = false;
                    break;
                }
            }
            if (canMoveHorizontal) {
                hero.moveLeft(moveStep);
            }
        } else if (horizontal == MovementHorizontal.RIGHT) {
            for (AbstractPaintable przeszkoda : przeszkody) {
                if (!hero.canMoveRight(przeszkoda)) {
                    canMoveHorizontal = false;
                    break;
                }
            }
            if (canMoveHorizontal) {
                hero.moveRight(moveStep);
            }
        }

        if (hero.checkCollision(finishGate)) {
            Dispatcher.instance.dispatch(new GateReachedEvent());
        }

        int pozycjaNaEkranie = hero.getPositionX() - drawOffset;
        if (pozycjaNaEkranie >= (wymiar.width / 2)) {
            int roznica = pozycjaNaEkranie - (wymiar.width / 2);
            drawOffset += roznica;
        }
    }

    public void verticalMovement(MovementVertical direction) {
        if (direction == MovementVertical.UP) {
            hero.jump();
        }
    }

    public void horizontalMovement(MovementHorizontal direction) {
        horizontal = direction;
    }

    @Override
    public void playerReachedGate() {
        level++;
        hero = new GameHero(300, 300);
        loadLevel();
        drawOffset = 0;
    }
}

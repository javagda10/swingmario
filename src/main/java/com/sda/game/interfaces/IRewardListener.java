package com.sda.game.interfaces;

import com.sda.game.models.AbstractPaintable;

public interface IRewardListener {
    void reward(AbstractPaintable paintable);
}

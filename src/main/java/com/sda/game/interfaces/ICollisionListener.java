package com.sda.game.interfaces;

import com.sda.game.models.AbstractPaintable;

public interface ICollisionListener {
    public void collisionVertical(AbstractPaintable with);
    public void collisionHorizontal(AbstractPaintable with);
}

package com.sda.game.interfaces;

public interface IFinishGateReachedListener {
    void playerReachedGate();
}

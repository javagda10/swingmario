package com.sda.game.events;

import com.sda.game.interfaces.ICollisionListener;
import com.sda.game.models.AbstractPaintable;

import java.util.List;

public class VerticalCollisionEvent implements IEvent {
    private AbstractPaintable collidedWith;

    public VerticalCollisionEvent(AbstractPaintable collidedWith) {
        this.collidedWith = collidedWith;
    }

    @Override
    public void execute() {
        List<ICollisionListener> list = Dispatcher.instance.getAllObjectsImplementingInterface(ICollisionListener.class);
        for (ICollisionListener listener : list) {
            listener.collisionVertical(collidedWith);
        }
    }

}

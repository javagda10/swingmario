package com.sda.game.events;

public interface IEvent {
    void execute();
}

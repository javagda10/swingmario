package com.sda.game.events;

import com.sda.game.interfaces.ICollisionListener;
import com.sda.game.interfaces.IRewardListener;
import com.sda.game.models.AbstractPaintable;

import java.util.List;

public class RewardCollisionEvent implements IEvent {
    private AbstractPaintable nagroda;
    public RewardCollisionEvent(AbstractPaintable paintable) {
        this.nagroda = paintable;
    }

    @Override
    public void execute() {
        List<IRewardListener> list = Dispatcher.instance.getAllObjectsImplementingInterface(IRewardListener.class);
        for (IRewardListener listener : list) {
            listener.reward(nagroda);
        }
    }
}

package com.sda.game.events;

import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Dispatcher {
    // singleton
    public static Dispatcher instance = new Dispatcher();

    // pula wÄ…tkÃ³w
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    /**
     * Mapa interfejsÃ³w (klas interfejsÃ³w) na obiekty ktÃ³re je implementujÄ…
     */
    private Map<Class<?>, List<?>> map = new HashMap<>();

    private Dispatcher() {
    }

    /**
     * Zarejestruj obiekt do dispatchera.
     *
     * @param o - obiekt do zarejestrowania
     */
    public void registerObject(Object o) {
        List<Class<?>> interfacesImplementedByObject = ClassUtils.getAllInterfaces(o.getClass());

        for (Class<?> classtype : interfacesImplementedByObject) {
//            System.out.println("Rejestruje obiekt: " + o + " - implementuje interfejs " + classtype.getName());
            List objects = map.get(classtype);
            if (objects == null) { // jeÅ›li lista nie istnieje (nie ma obiektu
                // ktÃ³ry implementuje ten interfejs)
                objects = new ArrayList<>(); // tworze nowa liste
            }
            objects.add(o); // dodaje obiekt do listy
            map.put(classtype, objects); // umieszczam listÄ™ z powrotem w mapie
        }
    }

    /**
     * Zwraca wszystkie obiekty ktÃ³re implementuja dany interfejs.
     *
     * @param clas - interfejs szukany
     * @return zwraca kolekcjÄ™ obiektÃ³w
     */
    public <T> List<T> getAllObjectsImplementingInterface(Class<T> clas) {
        List<T> lista = (List<T>) map.get(clas);
//        System.out.println("Szukam obiektÃ³w implementujÄ…cych interfejs: " + clas.getName() + " znalezione obiekty: ");
//        for (T t : lista) {
//            System.out.println(" ----> " + t);
//        }
//        System.out.println();

        return (List<T>) map.get(clas);
    }

    public void unregisterObject(Object o) {
        List<Class<?>> interfacesImplementedByObject = ClassUtils.getAllInterfaces(o.getClass()); // pobieram interfejsy ktÃ³re obiekt implementuje
        for (Class<?> classtype : interfacesImplementedByObject) { // dla kaÅ¼dego interfejsu
            List objects = map.get(classtype);  // pobieram listÄ™ obiektÃ³w ktÃ³re posiadam, ktÃ³re implementujÄ… ten interfejs (z tej listy chcÄ™ usunaÄ‡ obiekt o)
            if (objects != null) { // jeÅ›li lista nie istnieje ( to nie ma obiektu ktÃ³ry implementuje ten interfejs)
                objects.remove(o); // usuwam z listy
            }

        }
    }

    public void dispatch(final IEvent e) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    e.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
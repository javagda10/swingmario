package com.sda.game.events;

import com.sda.game.interfaces.ICollisionListener;
import com.sda.game.models.AbstractPaintable;

import java.util.List;

public class HorizontalCollisionEvent implements IEvent {
    private AbstractPaintable collidedWith;

    public HorizontalCollisionEvent(AbstractPaintable collidedWith) {
        this.collidedWith = collidedWith;
    }

    @Override
    public void execute() {
        List<ICollisionListener> list = Dispatcher.instance.getAllObjectsImplementingInterface(ICollisionListener.class);
        for (ICollisionListener listener : list) {
            listener.collisionHorizontal(collidedWith);
        }
    }

}
